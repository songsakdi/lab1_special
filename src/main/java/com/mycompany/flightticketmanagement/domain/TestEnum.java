/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.flightticketmanagement.domain;

/**
 *
 * @author User
 */
public class TestEnum {
    public static void main(String[] args) {
        for (Airport airport : Airport.values()){
            System.out.println(airport.name() + ":" + airport.airportName()
            + ":" + airport.city()+ ":" + airport.country()); 
        }
    }
    
}