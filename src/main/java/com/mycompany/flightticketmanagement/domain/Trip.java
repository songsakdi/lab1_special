/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.flightticketmanagement.domain;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.time.ZonedDateTime;


/**
 *
 * @author User
 */
public class Trip {
    
    private Airport fromAirPort ; 
    private Airport toAirport ;
    private ZonedDateTime departureDateTime ;
    private ZonedDateTime returnDateTime ;
    private boolean directFlight ;
    private byte noOfPassengers ; 
    private TravelClass travelClass ;
    private TripType tripType ;
    
    
}