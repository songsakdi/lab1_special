/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.flightticketmanagement.controller;

import com.mycompany.flightticketmanagement.domain.Flight;
import com.mycompany.flightticketmanagement.domain.Trip;
import java.util.List;


/**
 *
 * @author User
 */
public interface IFlightManagement {
    List<Flight> searchFlight(Trip trip) throws MissingRequiredTripInfoException ;
}
